import 'package:flutter/material.dart';

import 'package:contact_app/components/Button/Button.style.dart';

class Button extends StatelessWidget {
  Button({Key key, this.type, this.title, this.onPress}) : super(key: key);

  final String type;
  final String title;
  final Function onPress;

  final buttonColor = {
    'success': ButtonStyle.successColor,
    'danger': ButtonStyle.dangerColor
  };

  final splashColor = {
    'success': ButtonStyle.successSplashColor,
    'danger': ButtonStyle.dangerSplashColor
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
          color: buttonColor[type],
          textColor: Colors.white,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          padding: EdgeInsets.all(8.0),
          splashColor: splashColor[type],
          onPressed: onPress,
          child: Text(title),
        ),
    );
  }
}
