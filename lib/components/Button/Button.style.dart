import 'package:flutter/material.dart';

class ButtonStyle {
  static const successColor = Colors.green;
  static const dangerColor = Colors.red;
  static const successSplashColor = Colors.greenAccent;
  static const dangerSplashColor = Colors.redAccent;
}
