import 'package:flutter/material.dart';

import 'package:contact_app/components/Button/Button.dart';
import 'package:contact_app/screens/Routes.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: Center(
        child: Button(
          type: 'success',
          title: 'Go to Contact List',
          onPress: () => Navigator.pushNamed(context, Routes.contactList),
        ),
      )
    );
  }
}