class Routes {
  static const dashboard = '/';
  static const contactList = '/contacts';
  static const contactDetail = '/contact/detail';
}