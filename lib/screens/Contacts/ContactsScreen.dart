import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:contact_app/screens/Contacts/Components/ContactList/ContactList.dart';

class Contacts {
  final List contacts;

  Contacts({ this.contacts });

  factory Contacts.onGetResponse(List<dynamic> response) {
    return Contacts(contacts: response);
  }
}

Future<Contacts> fetchContact() async {
  final response =
      await http.get('https://my-json-server.typicode.com/HongkyBayu/fakeServer/contacts');

  if (response.statusCode == 200) {
    return Contacts.onGetResponse(json.decode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

class ContactsScreen extends StatefulWidget {
  ContactsScreen({ Key key }) : super(key : key);

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  Future<Contacts> contacts;

  @override
  void initState() {
    super.initState();
    contacts = fetchContact();
  }

  Future<void> _refreshContact() async {
    setState(() {
      contacts = fetchContact();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contacts'),
      ),
      body: FutureBuilder<Contacts>(
        future: contacts,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return RefreshIndicator(child: ContactList(data: snapshot.data), onRefresh: _refreshContact );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error....'),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      )
    );
  }
}