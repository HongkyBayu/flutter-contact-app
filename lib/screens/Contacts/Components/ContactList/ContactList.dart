import 'package:flutter/material.dart';

import 'package:contact_app/screens/Contacts/Components/ContactTile/ContactTile.dart';
import 'package:contact_app/screens/Contacts/ContactsScreen.dart';

class ContactList extends StatelessWidget {
  ContactList({ this.data });
  final Contacts data;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        var contact = data.contacts[index];

        return ContactTile(
            contact: contact,
          );
        },
      itemCount: data.contacts.length,
    );
  }
}
