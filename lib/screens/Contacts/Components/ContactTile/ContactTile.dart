import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;

import 'package:contact_app/screens/ScreenArgument.dart';
import 'package:contact_app/screens/Routes.dart';

class ContactTile extends StatelessWidget {
  ContactTile({ Key key, this.contact }) : super(key : key);

  final Map contact;

  genarateMd5(String data) {
    var content = new Utf8Encoder().convert(data);
    var md5 = crypto.md5;
    var digest = md5.convert(content);

    return hex.encode(digest.bytes);
  }

  @override
  Widget build(BuildContext context) {
    var hashedEmail = genarateMd5(contact['email']);
    var imageUri = "http://www.gravatar.com/avatar/$hashedEmail";
    var navigationParams = {
      "name": contact["name"],
      "email": contact["email"],
      "mobile": contact["mobile"],
      "avatar": imageUri
    };

    return Card(
      child: ListTile(
        leading: Image.network(
          "$imageUri?s=50",
        ),
        title: Text(contact['name']),
        subtitle: Text(contact['mobile']),
        onTap: () { 
          Navigator.pushNamed(
            context, 
            Routes.contactDetail,
            arguments: ScreenArgument(navigationParams)
          );
        },
      )
    );
  }
}