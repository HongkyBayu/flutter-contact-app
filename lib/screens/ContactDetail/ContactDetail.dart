import 'package:flutter/material.dart';

import 'package:contact_app/screens/ScreenArgument.dart';

class ContactDetail extends StatefulWidget {
  ContactDetail({ Key key }) : super(key: key);

  @override
  _ContactDetailState createState() => _ContactDetailState();
}

class _ContactDetailState extends State<ContactDetail> {
  bool isFavourite = false;

  void onToggleFAB() {
    setState(() {
      isFavourite = !isFavourite;
    });
  }

  @override
  Widget build(BuildContext context) {
    final ScreenArgument args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(args.navigationParams['name']),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            _ContactAvatarWidget(imageUri: args.navigationParams['avatar']),
            _ContactDetailInformation(label: 'Mobile', content: args.navigationParams['mobile'],),
            _ContactDetailInformation(label: 'Email', content: args.navigationParams['email'])
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: onToggleFAB,
        child: Icon(
          isFavourite ? Icons.favorite : Icons.favorite_border,
          color: isFavourite ? Colors.red : null,
        ),
      )
    );
  }
}

class _ContactAvatarWidget extends StatelessWidget {
  _ContactAvatarWidget({ this.imageUri });

  final String imageUri;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(top: 15),
        child: ClipOval(
          child: Image.network(
            '$imageUri?s=250',
            height: 250,
            width: 250,
            fit: BoxFit.fill,
          ),
        )
      ),
    );
  }
}

class _ContactDetailInformation extends StatelessWidget {
  _ContactDetailInformation({ this.label, this.content });
  
  final String label;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(top: 30, right: 20, left: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              label,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 15.0,
                fontFamily: 'Campton_Light',
              ),
            ),
            Text(
              content,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w200,
                fontSize: 15.0,
                fontFamily: 'Campton_Light',
              )
            )
          ],
        ),
      )
    );
  }
}