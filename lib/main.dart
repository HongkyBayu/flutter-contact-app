import 'package:flutter/material.dart';

import 'package:contact_app/screens/Dashboard/DashboardScreen.dart';
import 'package:contact_app/screens/Contacts/ContactsScreen.dart';
import 'package:contact_app/screens/ContactDetail/ContactDetail.dart';
import 'package:contact_app/screens/Routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contact App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: Routes.dashboard,
      routes: {
        Routes.dashboard: (context) => DashboardScreen(),
        Routes.contactList: (context) => ContactsScreen(),
        Routes.contactDetail: (context) => ContactDetail()
      }
    );
  }
}